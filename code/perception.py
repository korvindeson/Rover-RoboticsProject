import numpy as np
import cv2

# Identify pixels above the threshold
# Threshold of RGB > 160 does a nice job of identifying ground pixels only
def color_thresh(img, rgb_thresh=(160, 160, 160)):
    # Create an array of zeros same xy size as img, but single channel
    color_select = np.zeros_like(img[:,:,0])
    # Require that each pixel be above all three threshold values in RGB
    # above_thresh will now contain a boolean array with "True"
    # where threshold was met
    above_thresh = (img[:,:,0] > rgb_thresh[0]) \
                & (img[:,:,1] > rgb_thresh[1]) \
                & (img[:,:,2] > rgb_thresh[2])
    # Index the array of zeros with the boolean array and set to 1
    color_select[above_thresh] = 1
    # Return the binary image
    return color_select

def rock_thresh(img):
    color_select = np.zeros_like(img[:,:,0])
    
    lower = np.array([0, 0, 0], dtype = "uint8")
    upper = np.array([20, 20, 20], dtype = "uint8")
    
    # find the colors within the specified boundaries and apply
    # the mask
    mask = cv2.inRange(img, lower, upper)
    output = cv2.bitwise_and(img, img, mask = mask)
    
    above_thresh = (output[:,:,0] > 0) \
                & (output[:,:,1] > 0) \
                & (output[:,:,2] > 0)
    color_select[above_thresh] = 1
    return color_select


# Define a function to convert to rover-centric coordinates
def rover_coords(binary_img):
    # Identify nonzero pixels
    if len(binary_img.nonzero()) > 2:
        ypos, xpos, zpos = binary_img.nonzero()
    else:
        ypos, xpos = binary_img.nonzero()
    # Calculate pixel positions with reference to the rover position being at the 
    # center bottom of the image.  
    x_pixel = np.absolute(ypos - binary_img.shape[0]).astype(np.float)
    y_pixel = -(xpos - binary_img.shape[0]).astype(np.float)
    return x_pixel, y_pixel


# Define a function to convert to radial coords in rover space
def to_polar_coords(x_pixel, y_pixel):
    # Convert (x_pixel, y_pixel) to (distance, angle) 
    # in polar coordinates in rover space
    # Calculate distance to each pixel
    dist = np.sqrt(x_pixel**2 + y_pixel**2)
    # Calculate angle away from vertical for each pixel
    angles = np.arctan2(y_pixel, x_pixel)
    return dist, angles

# Define a function to apply a rotation to pixel positions
def rotate_pix(xpix, ypix, yaw):
    # TODO:
    # Convert yaw to radians
    # Apply a rotation
    yaw=float(yaw)*np.pi/180
    xpix_rotated = xpix*np.cos(yaw)-ypix*np.sin(yaw)
    ypix_rotated = xpix*np.sin(yaw)+ypix*np.cos(yaw)
    # Return the result  
    return xpix_rotated, ypix_rotated

# Define a function to perform a translation
def translate_pix(xpix_rot, ypix_rot, xpos, ypos, scale): 
    # TODO:
    # Apply a scaling and a translation
    xpix_translated = np.int_(xpos+(xpix_rot/scale))
    ypix_translated = np.int_(ypos+(ypix_rot/scale))
    # Return the result  
    return xpix_translated, ypix_translated

# Define a function to apply rotation and translation (and clipping)
# Once you define the two functions above this function should work
def pix_to_world(xpix, ypix, xpos, ypos, yaw, world_size, scale):
    # Apply rotation
    xpix_rot, ypix_rot = rotate_pix(xpix, ypix, yaw)
    # Apply translation
    xpix_tran, ypix_tran = translate_pix(xpix_rot, ypix_rot, xpos, ypos, scale)
    # Perform rotation, translation and clipping all at once
    x_pix_world = np.clip(np.int_(xpix_tran), 0, world_size - 1)
    y_pix_world = np.clip(np.int_(ypix_tran), 0, world_size - 1)
    # Return the result
    return x_pix_world, y_pix_world

# Define a function to perform a perspective transform
def perspect_transform(img, src, dst):
           
    M = cv2.getPerspectiveTransform(src, dst)
    warped = cv2.warpPerspective(img, M, (img.shape[1], img.shape[0]))# keep same size as input image
    
    return warped


# Apply the above functions in succession and update the Rover state accordingly
def perception_step(Rover):
    # Perform perception steps to update Rover()
    # TODO: 
    # NOTE: camera image is coming to you in Rover.img
    # 1) Define source and destination points for perspective transform
    source = np.float32([[4, 138],
                         [279, 138],
                         [192, 96],
                         [112,96]])

    
    dst_size = 5 
    bottom_offset = 3
    
    destination = np.float32([[Rover.img.shape[1]/2 - dst_size, Rover.img.shape[0] - bottom_offset],
                  [Rover.img.shape[1]/2 + dst_size, Rover.img.shape[0] - bottom_offset],
                  [Rover.img.shape[1]/2 + dst_size, Rover.img.shape[0] - 2*dst_size - bottom_offset], 
                  [Rover.img.shape[1]/2 - dst_size, Rover.img.shape[0] - 2*dst_size - bottom_offset],
                  ])
    
    # 2) Apply perspective transform
    
    warped = perspect_transform(dst=destination,src=source,img=Rover.img)
    
    # 3) Apply color threshold to identify navigable terrain/obstacles/rock samples
    tr = color_thresh(warped)
    rock = rock_thresh(warped)
    # 4) Update Rover.vision_image (this will be displayed on left side of screen)
        # Example: Rover.vision_image[:,:,0] = obstacle color-thresholded binary image
        #          Rover.vision_image[:,:,1] = rock_sample color-thresholded binary image
        #          Rover.vision_image[:,:,2] = navigable terrain color-thresholded binary image
    
    xpix,ypix = rover_coords(tr)
    xpix_rock,ypix_rock = rover_coords(rock)
    
    x_world_rock, y_world_rock = pix_to_world(xpix_rock, ypix_rock, float(Rover.pos[0]), 
                                                                      float(Rover.pos[1]), 
                                                                      float(Rover.yaw), Rover.worldmap.shape[0], 10)
    


    # 6) Convert rover-centric pixel values to world coordinates

    warped = perspect_transform(Rover.img, source, destination)

    lower = np.array([180, 0, 0], dtype = "uint8")
    upper = np.array([255, 255, 140], dtype = "uint8")
    
    # find the colors within the specified boundaries and apply
    # the mask
    mask = cv2.inRange(warped, lower, upper)
    output = cv2.bitwise_and(warped, warped, mask = mask)
    
    above_thresh =    (output[:,:,0] > 0) \
                    & (output[:,:,1] > 0) \
                    & (output[:,:,2] > 0)
        
    

    scale=20
    Rover.pos = pix_to_world(xpix, ypix, float(Rover.pos[0]), 
                                              float(Rover.pos[1]), 
                                              float(Rover.yaw), Rover.worldmap.shape[0], scale)
    
    if len(output[above_thresh]) > 10:
        xpix,ypix = rover_coords(output)
        Rover.nav_angles_old = Rover.nav_angles
        Rover.nav_dists, Rover.nav_angles =  to_polar_coords(xpix,ypix)
        
        if Rover.vel < 1.:
            Rover.throttle = 3
    
        if Rover.picking_up != 0 and Rover.onetime == True:
            Rover.onetime=False
            
        if Rover.picking_up == 0 and Rover.onetime == False and Rover.near_sample == 0 and Rover.send_pickup == False and Rover.vel > 0.1:
            Rover.onetime=True
            Rover.mode = 'turn'
    else:
        Rover.nav_angles_old = Rover.nav_angles
        Rover.nav_dists, Rover.nav_angles =  to_polar_coords(xpix,ypix)
    
    
    # 7) Update Rover worldmap (to be displayed on right side of screen)
    #Rover.worldmap[obstacle_y_world, obstacle_x_world, 0] += 1
    #Rover.worldmap[ Rover.pos[1]-5,  Rover.pos[0]+5, 0] += 1
    #Rover.worldmap[ Rover.pos[1]-10,  Rover.pos[0]+10, 0] += 1
    Rover.worldmap[ Rover.pos[1],  Rover.pos[0], 2] += 1
    Rover.worldmap[y_world_rock,  x_world_rock, 1] += 1
    
    
    output_image = np.zeros((Rover.img.shape[0], Rover.img.shape[1]*2, 3))
    
        # Next you can populate regions of the image with various output
        # Here I'm putting the original image in the upper left hand corner
    output_image[0:Rover.img.shape[0], 0:Rover.img.shape[1]] = warped
    
        # Let's create more images to add to the mosaic, first a warped image
    warped = perspect_transform(Rover.img, source, destination)
        # Add the warped image in the upper right hand corner
    output_image[0:Rover.img.shape[0], Rover.img.shape[1]:] = output

        # Overlay worldmap with ground truth map
    #map_add = cv2.addWeighted(Rover.worldmap, 1, Rover.ground_truth, 0.5, 0)
    
    
    #output_image[Rover.img.shape[0]:, 0:Rover.worldmap.shape[1]] = np.flipud(map_add)

    Rover.vision_image = output_image
    
    return Rover