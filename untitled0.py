# -*- coding: utf-8 -*-
"""
Created on Fri May 26 23:47:10 2017

@author: moroz
"""

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
# Uncomment the next line for use in a Jupyter notebook
# This enables the interactive matplotlib window
#%matplotlib notebook
image = mpimg.imread('./rover/img/robocam_2017_05_27_09_19_07_115.jpg')
plt.imshow(image)
plt.show()

